import grpc

import sys
sys.path.append('/Users/hx/go/src/bitbucket.org/hxhxhx88/common/proto')

import model_service_pb2
import model_service_pb2_grpc

def run():
    channel = grpc.insecure_channel('localhost:3456')
    stub = model_service_pb2_grpc.ModelStub(channel)

    req = model_service_pb2.PredictRequest()

    with open("flowertest.jpg", "rb") as imageFile:
        f = imageFile.read()
        b = bytearray(f)
    req.image = bytes(b)

    resp = stub.Predict(req)

    for pred in resp.predictions:
        print "%s %s" % (pred.label, pred.probability)

if __name__ == "__main__":
    run()
