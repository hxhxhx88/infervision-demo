Generate proto buffer for Go:

```shell
$ PROTO_DIR=$GOPATH/src/bitbucket.org/hxhxhx88/common/proto; protoc -I $PROTO_DIR $PROTO_DIR/model_service.proto --go_out=plugins=grpc:$PROTO_DIR
```

Generate proto buffer for Python:

```shell
$ PROTO_DIR=$GOPATH/src/bitbucket.org/hxhxhx88/common/proto; python -m grpc_tools.protoc -I $PROTO_DIR --python_out=$PROTO_DIR --grpc_python_out=$PROTO_DIR $PROTO_DIR/model_service.proto
```