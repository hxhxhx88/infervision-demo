package main

import (
	"encoding/base64"
	"flag"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"

	pb "bitbucket.org/hxhxhx88/common/proto"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

var indexTemplate = template.Must(template.ParseFiles(
	"view/index.html",
	"view/layout.html",
))

type indexData struct {
	ImageBase64 string
	Predictions []*pb.Prediction
}

var currPrediction []*pb.Prediction
var currImageBase64 string

func index(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s %s\n", r.Method, r.URL.String())

	var data indexData
	data.Predictions = currPrediction
	data.ImageBase64 = currImageBase64

	currPrediction = make([]*pb.Prediction, 0)
	currImageBase64 = ""

	indexTemplate.ExecuteTemplate(w, "layout.html", data)
}

func predict(w http.ResponseWriter, r *http.Request) {
	log.Printf("%s %s\n", r.Method, r.URL.String())

	r.ParseMultipartForm(0)

	file, _, e1 := r.FormFile("image")
	if e1 != nil {
		http.Error(w, e1.Error(), http.StatusBadRequest)
		return
	}
	defer file.Close()

	imageBytes, e2 := ioutil.ReadAll(file)
	if e2 != nil {
		http.Error(w, e2.Error(), http.StatusBadRequest)
		return
	}

	var req pb.PredictRequest
	req.Image = imageBytes

	resp, e3 := grpcClient.Predict(context.Background(), &req)
	if e3 != nil {
		http.Error(w, e3.Error(), http.StatusBadRequest)
		return
	}

	currPrediction = resp.Predictions
	currImageBase64 = base64.StdEncoding.EncodeToString(imageBytes)
	http.Redirect(w, r, "/", http.StatusMovedPermanently)
}

var grpcClient pb.ModelClient

func initWebServer() {
	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.HandleFunc("/predict/", predict)
	http.HandleFunc("/", index)
}

func main() {
	grpcHostPtr := flag.String("grpc-host", "localhost", "host to the gRPC server")
	grpcPortPtr := flag.Int("grpc-port", 1234, "port to the gRPC server")
	portPtr := flag.Int("p", 8080, "port")
	flag.Parse()

	// init gRPC
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	conn, e1 := grpc.Dial(fmt.Sprintf("%s:%d", *grpcHostPtr, *grpcPortPtr), opts...)
	if e1 != nil {
		log.Fatal(e1)
	}
	grpcClient = pb.NewModelClient(conn)
	log.Printf("Connect to gRPC at %s:%d", *grpcHostPtr, *grpcPortPtr)

	// init web server
	initWebServer()

	log.Printf("Listen on :%d", *portPtr)
	if e := http.ListenAndServe(fmt.Sprintf(":%d", *portPtr), nil); e != nil {
		log.Fatal(e)
	}
}
