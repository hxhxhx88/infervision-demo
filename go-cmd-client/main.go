package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"

	pb "bitbucket.org/hxhxhx88/common/proto"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func main() {
	jpgPathPtr := flag.String("jpg", "", "path to the jpeg file")
	grpcHostPtr := flag.String("h", "localhost", "host to the gRPC server")
	grpcPortPtr := flag.Int("p", 1234, "port to the gRPC server")
	flag.Parse()

	jpgPath := *jpgPathPtr
	if jpgPath == "" {
		flag.PrintDefaults()
		return
	}

	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	conn, e1 := grpc.Dial(fmt.Sprintf("%s:%d", *grpcHostPtr, *grpcPortPtr), opts...)
	if e1 != nil {
		log.Fatal(e1)
	}
	defer conn.Close()

	client := pb.NewModelClient(conn)

	imageBytes, e3 := ioutil.ReadFile(jpgPath)
	if e3 != nil {
		log.Fatal(e3)
	}

	var req pb.PredictRequest
	req.Image = imageBytes

	resp, e2 := client.Predict(context.Background(), &req)
	if e2 != nil {
		log.Fatal(e2)
	}

	for _, pred := range resp.Predictions {
		fmt.Printf("%s: %f\n", pred.Label, pred.Probability)
	}
}
