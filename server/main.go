package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"image/jpeg"
	"io/ioutil"
	"log"
	"net"
	"path/filepath"
	"sort"
	"strings"

	pb "bitbucket.org/hxhxhx88/common/proto"
	"github.com/anthonynsimon/bild/transform"
	"github.com/songtianyi/go-mxnet-predictor/mxnet"
	"github.com/songtianyi/go-mxnet-predictor/utils"
	"google.golang.org/grpc"
)

type modelServer struct {
	labels []string
	nd     *mxnet.NDList
	item   *mxnet.NDItem
	p      *mxnet.Predictor
}

func (s modelServer) close() {
	s.nd.Free()
	s.p.Free()
}

func newServer(modelDir string) (s *modelServer, err error) {
	// load mean image from file
	nd, e1 := mxnet.CreateNDListFromFile(filepath.Join(modelDir, "mean.bin"))
	if e1 != nil {
		err = e1
		return
	}

	// get mean image data from C memory
	item, e2 := nd.Get(0)
	if e2 != nil {
		err = e2
		return
	}

	// load model
	symbol, e3 := ioutil.ReadFile(filepath.Join(modelDir, "102flowers-symbol.json"))
	if e3 != nil {
		err = e3
		return
	}
	params, e4 := ioutil.ReadFile(filepath.Join(modelDir, "102flowers-0260.params"))
	if e4 != nil {
		err = e4
		return
	}

	// create predictor
	p, e5 := mxnet.CreatePredictor(symbol,
		params,
		mxnet.Device{Type: mxnet.CPU_DEVICE, Id: 0},
		[]mxnet.InputNode{{Key: "data", Shape: []uint32{1, 3, 299, 299}}},
	)
	if e5 != nil {
		err = e5
		return
	}

	// load labels
	labelsBytes, e6 := ioutil.ReadFile(filepath.Join(modelDir, "102flowers-labels.txt"))
	if e6 != nil {
		err = e6
		return
	}
	labels := strings.Split(string(labelsBytes), "\n")
	if len(labels) != 102 {
		err = fmt.Errorf("%d label loaded, rather than 102", len(labels))
		return
	}

	var server modelServer
	server.labels = labels
	server.item = item
	server.p = p
	server.nd = nd

	s = &server

	return
}

func (s *modelServer) predict(imageBytes []byte) (preds []*pb.Prediction, err error) {
	reader := bytes.NewReader(imageBytes)

	// load test image for predction
	img, e1 := jpeg.Decode(reader)
	if e1 != nil {
		err = e1
		return
	}

	// preprocess
	resized := transform.Resize(img, 299, 299, transform.Linear)
	res, e2 := utils.CvtImageTo1DArray(resized, s.item.Data)
	if e2 != nil {
		err = e2
		return
	}

	// set input
	if e := s.p.SetInput("data", res); e != nil {
		err = e
		return
	}

	// do predict
	if e := s.p.Forward(); e != nil {
		err = e
		return
	}

	// get predict result
	data, err := s.p.GetOutput(0)
	if err != nil {
		return
	}

	idxs := make([]int, len(data))
	for i := range data {
		idxs[i] = i
	}
	as := utils.ArgSort{Args: data, Idxs: idxs}
	sort.Sort(as)

	num := 3
	for i := 0; i < num; i++ {
		var pred pb.Prediction
		pred.Label = s.labels[as.Idxs[i]]
		pred.Probability = float64(as.Args[i])
		preds = append(preds, &pred)
	}

	return
}

func (s modelServer) Predict(ctx context.Context, req *pb.PredictRequest) (respPtr *pb.PredictResponse, err error) {
	preds, e1 := s.predict(req.Image)
	if e1 != nil {
		err = e1
		return
	}

	var resp pb.PredictResponse
	resp.Predictions = append(resp.Predictions, preds...)

	respPtr = &resp

	return
}

func main() {
	portPtr := flag.Int("p", 1234, "port")
	modelDirPtr := flag.String("dir", "", "model dir")
	flag.Parse()

	modelDir := *modelDirPtr
	if modelDir == "" {
		flag.PrintDefaults()
		return
	}

	s, err := newServer(modelDir)
	if err != nil {
		log.Fatal(err)
	}
	defer s.close()

	grpcServer := grpc.NewServer()
	pb.RegisterModelServer(grpcServer, s)

	// Don't use localhost!
	lis, e1 := net.Listen("tcp", fmt.Sprintf(":%d", *portPtr))
	if e1 != nil {
		log.Fatal(e1)
	}
	grpcServer.Serve(lis)
}
