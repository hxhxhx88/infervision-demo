Build:

```shell
$ docker build -t server .
```

Run:

```shell
$ docker run -p 3456:2345 -v /path/to/flowers/model/dir:/data/flowers server
```

Replace `/path/to/flowers/model/dir` to the absolute path of the directory containing

- 102flowers-0260.params
- 102flowers-symbol.json
- mean.bin
- 102flowers-labels.txt

The first three files can be downloaded from [here](https://github.com/songtianyi/go-mxnet-predictor#23-download-example-files).

The file `102flowers-labels.txt` is attached in the repo at [here](/data/102flowers-labels.txt), which is converted from [this file](https://github.com/jimgoo/caffe-oxford102/blob/master/class_labels.py).
